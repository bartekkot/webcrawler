import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


/**
 * Created by x on 5/19/17.
 */
public class FileToStringConverter {


    public StringBuffer readFromFile(String path) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new FileReader(path));

        StringBuffer stringBuffer = new StringBuffer();
        String line = null;

        while ((line = bufferedReader.readLine()) != null) {

            stringBuffer.append(line).append("\n");
        }
        return stringBuffer;
    }



    public String[] toStringArray(StringBuffer sb) {
        String str = sb.toString();
        String[] stringArray = str.split("\\r?\\n");

        return stringArray;
    }

}
