/**
 * Created by x on 5/19/17.
 */
public class StringCounter {

    public int stringCounter(String text, String searchUnit){

        text = text.toLowerCase();
        searchUnit = searchUnit.toLowerCase();

        int lastIndex = 0;
        int count = 0;

        while (lastIndex != -1) {

            lastIndex = text.indexOf(searchUnit, lastIndex);

            if (lastIndex != -1) {
                count++;
                lastIndex += searchUnit.length();
            }
        }
        return count;
    }

}
