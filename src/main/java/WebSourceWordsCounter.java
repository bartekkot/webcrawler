import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by x on 5/20/17.
 */
public class WebSourceWordsCounter {

    FileToStringConverter converter;
    WebPageSourceExtractor extractor;
    StringCounter counter;

        public void initializeWebWordsCounting(String path, String word) throws IOException  {
            converter = new FileToStringConverter();
            extractor = new WebPageSourceExtractor();
            counter = new StringCounter();
            converter.toStringArray(converter.readFromFile(path));
            //System.out.println(converter.toStringArray(converter.readFromFile(path))[2]);
            String html = "";
            for(int i = 0; i< converter.toStringArray(converter.readFromFile(path)).length; i++ ){
                html = html + extractor.getUrlSource(converter.toStringArray(converter.readFromFile(path))[i]);
            }

            Document doc = Jsoup.parse(html);
            String text = doc.body().text();
            //System.out.println(text);


            System.out.println(counter.stringCounter(text, word));
        }
}
